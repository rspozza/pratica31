
import java.util.Date;
import java.util.GregorianCalendar;

/**
 *
 * @author pozza
 */
public class Pratica31 {

    private static String meuNome = "Rogerio Santos Pozza", part1, part2, part3, resultadoNome;

    private static Date inicio, fim;

    public static void main(String[] args) {
        inicio = new Date();

        System.out.println("Nome = " + meuNome.toUpperCase());

        part1 = meuNome.substring(16, 20).toLowerCase();
        part1 = String.valueOf(meuNome.charAt(15)).toUpperCase() + part1;

        part2 = String.valueOf(meuNome.charAt(0)).toUpperCase();

        part3 = String.valueOf(meuNome.charAt(8)).toUpperCase();

        resultadoNome = part1 + ", " + part2 + ". " + part3 + ".";

        System.out.println("Indice = " + resultadoNome);
        GregorianCalendar dataNascimento = new GregorianCalendar(1972, 00, 10);
        GregorianCalendar dataAtual = new GregorianCalendar(2016, 9, 1);

        long M_S_DIA = 1000 * 60 * 60 * 24;
        long i = dataAtual.getTimeInMillis() - dataNascimento.getTimeInMillis();

        System.out.println("Idade em dias = " + i / M_S_DIA);

        fim = new Date();

        System.out.println("Tempo decorrido = " + (fim.getTime() - inicio.getTime()));

    }

}
